LandauerHeatCurrent
===================

Calculation of the single-particle scattering (Landauer) expression for the heat current through a set of normal modes, locally coupled on either side to non-Markovian heat baths.

Modes are (possibly) coupled via the system-bath coupling, which is diagonal in the local basis. This is the reason that normal mode transmission through the system-bath interfaces is smaller than unity.

This solution revisits [Segal, Nitzan and Hanggi, (2003)](http://dx.doi.org/10.1063/1.1603211), but the current approach employs the Boson Green's operator formalism via the phonon counterpart of the [Meir-Wingreen formula (1992)](http://dx.doi.org/10.1103/PhysRevLett.68.2512) for the transmission, and addresses an inconsistency in the 2003 paper by using [a system-bath coupling model which is consistent at the strong-coupling limit](https://bitbucket.org/heat-conduction-mol-junctions/frequency-dependence-of-system-bath-coupling).

# Components
1. Single temperature set: `LandauerHeatCurrent-*.out`
    * Auto-generated on-line documentation can be found at `doc/html/index.html`, or alternatively in the reference manual, at `doc/latex/refman.pdf`.
2. Temperature scan: `LndrHtCnd-*.sh`

# Single temperature set
## Installation

```shell
mkdir -p bin
g++ src/LandauerHeatCurrent.cpp -I /usr/include/eigen3 -g3 -O3 -pg -Wall -Wextra -o bin/LandauerHeatCurrent-rc.out
```

### Developer Note
This C++ code is currently not object-oriented.

## Input
`LandauerHeatCurrent-*.out` takes a single `input.txt` file containing the following information:

1. Number of atoms
2. Debye frequency
3. System-bath coupling
4. Bath temperatures
5. Eigenfrequencies
6. Eigenvector coefficient in the Cartesian atomic basis
7. Frequency sampling interval
8. Number of frequencies to sample

* All energy or frequency values in radians per picosecond
    * 1 Kelvin = 0.130920 radians per picosecond
    * 1 wavenumber = 0.188496 radians per picosecond

## Output
1. Time-averaged current
2. Time-averaged conductance
3. Differential conductance

* Current in Watts.
* Conductance in pico-Watts per Kelvin.

# Temperature scan
## Input
`LndrHtCnd-*.sh` takes the same format `input.txt`, but ignores some of the fields:

1. Bath temperatures are scanned over (hard-coded values).
2. Frequency sample (interval/number) are discerned from the lowest and highest frequencies (usually the system-bath coupling, and highest molecular vibration frequency, respectively).
## Running
1. Running on the local host

```bash
LndrHtCnd-*.sh
```

2. Running on a remote compute node

```bash
qsub LndrHtCnd-*.sh
```

## Output
### Conductance temperature dependence
* `KvsT.jpg`
### Contributions to conductance
1. `FrequencyDependence.jpg`:
    1. Frequency dependence of the junction transmission and density of states
        1. Bath Density Of States (DOS)
        2. Molecular Vibrations Density Of States (VDOS)
        3. Junction transmission function
    2. Frequency dependence of the heat current at various temperatures
2. `Non-interactingModes.jpg`
    1. Contribution to diagonal part of the transmission, at each frequency, by each mode. With an inset detailing transmission (including mode coupling) at low frequencies.
    2. The same for the heat transfer function (i.e. transmission times frequency).

# Citing and contributing
* Owner: @inon_s