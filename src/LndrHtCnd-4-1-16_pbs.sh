#! /bin/bash -e
###PBS -q nano1
#PBS -N LndrHtCnd
#PBS -o LndrHtCnd.io
#PBS -e LndrHtCnd.err
#PBS -M inonshar@tau.ac.il
### send e-mail on abort or exit
###PBS -m ae 
#PBS -m a


function remoteNd()
{
echo
echo ==============================
echo PBS_JOBID = $PBS_JOBID
echo PBS_JOBNAME = $PBS_JOBNAME
echo PBS_QUEUE = $PBS_QUEUE
echo
echo HOST = $HOST
echo 'hostname' = `hostname`
echo PBS_O_HOST = $PBS_O_HOST
echo PBS_SERVER = $PBS_SERVER
echo
echo USER = $USER
echo LOGNAME = $LOGNAME
echo PBS_O_LOGNAME = $PBS_O_LOGNAME
echo
echo SHELL = $SHELL
echo PBS_O_SHELL = $PBS_O_SHELL
echo
echo HOME = $HOME
echo PBS_O_HOME = $PBS_O_HOME
echo
echo PWD = $PWD
echo 'pwd' = `pwd`
echo PBS_O_WORKDIR = $PBS_O_WORKDIR
echo PBS_O_INITDIR = $PBS_O_INITDIR
echo ==============================
echo
}

function check4Recompilation(){

binLocation="$HOME/LandauerHeatCurrent/bin/LandauerHeatCurrent-rc.out"
if [ ! -f $binLocation ] ; then echo "missing binary executable! Aborting..." ; exit 12 ; fi

#echo "[Epoch] modified changed"
#echo -n "LandauerHeatCurrent.cpp " $(stat -t ~/LandauerHeatCurrent/LandauerHeatCurrent.cpp | cut -d " " -f13-14) ; echo
#echo -n "functions.h " $(stat -t ~/LandauerHeatCurrent/functions.h | cut -d " " -f13-14) ; echo
#if [ -f $binLocation ]
#	then 
#	echo -n "LandauerHeatCurrent-rc.out " $(stat -t $binLocation | cut -d " " -f13-14) ; echo
#fi

echo "[Epoch] modified"
mainSource=$(stat -t ~/LandauerHeatCurrent/src/LandauerHeatCurrent.cpp | cut -d " " -f13)
echo -n "LandauerHeatCurrent.cpp " $mainSource ; echo
functionsSource=$(stat -t ~/LandauerHeatCurrent/src/functions.h | cut -d " " -f13)
echo -n "functions.h " $functionsSource ; echo
if [ -f $binLocation ]
       then 
				rmCompiled=$(stat -t $binLocation | cut -d " " -f13)
       echo -n "$binLocation " $rmCompiled ; echo
elif [ -f LandauerHeatCurrent-rc.out ]
       then 
        rmCompiled=$(stat -t LandauerHeatCurrent-rc.out | cut -d " " -f13)
       echo -n "./LandauerHeatCurrent-rc.out " $rmCompiled ; echo
else
	rmCompiled=0
	echo "LandauerHeatCurrent-rc.out found neither in the main directory nor the local one."
fi
if [ $mainSource -gt $rmCompiled ] || [ $functionsSource -gt $rmCompiled ]
	then
	echo "Compiled release-candidate is superceded by one of the source files."
	if [ "hydrogen.tau.ac.il" == $(hostname) ]
		then
		echo "Setting recompile flag, and continuing..."
		bRecompile=1
		bOverwriteCPP=1
	else
		echo "Recompile needed. Aborting."
		exit 87
	fi
else
	echo "Compilation up-to-date, continuing..."
	if [ -f $binLocation ]
		then
		sleep 2
		cp $binLocation .
	fi
fi
}


function recompile()
{
echo "______________"
echo "Recompiling..."

if [ 1 != $bOverwriteCPP ] && [ -f ~/LandauerHeatCurrent/LandauerHeatCurrent*.out ] ; then echo "LandauerHeatCurrent*.out already exists! back it up before running again... aborting!" ; exit 103 ; fi
# recompiling
set +e
CPP_ARGS="$HOME/LandauerHeatCurrent/src/LandauerHeatCurrent.cpp -lm -I /usr/include/eigen3/ -g -pg -O0 -Wall -Wextra -pedantic -o $HOME/LandauerHeatCurrent/LandauerHeatCurrent-alpha.out" # -v
echo "compiling debug version: g++ $CPP_ARGS"
g++ $CPP_ARGS
set -e
CPP_ARGS="$HOME/LandauerHeatCurrent/src/LandauerHeatCurrent.cpp -lm -I /usr/include/eigen3/ -g -O3 -o $binLocation"
echo "compiling production version: g++ $CPP_ARGS"
g++ $CPP_ARGS
if [ 0 -ne $? ] ; then echo "Release-candidate recompilation exited with errors. Check compilation log before re-running. Aborting..." ; exit 2987 ; fi
sleep 1
cp $binLocation .
}

function doSingleOscillatorFrequency()
{
	OFREQ=$[ $i * $DFREQ ] # calculate next Oscillator FREQuency
        echo -n "iteration $i "
        #sed -i '5s/.*/0\ '$OFREQ'/' input.txt # replace entire 5-th line of input.txt with "0 $OFREQ"
        #sed -i '5s/.*/'$OFREQ'\ 6/' input.txt # replace entire 5-th line of input.txt with "$OFREQ 6"
        sed -i '5s/.*/'$OFREQ'/' input.txt     # replace entire 5-th line of input.txt with "$OFREQ"
        #head input.txt -n5 | tail -n1 | cut -d " " -f1  # print out current oscillator frequency from input.txt file
        head input.txt -n5 | tail -n1                   # print out current oscillator frequency from input.txt file

        #echo -n $(head input.txt -n5 | tail -n1 | cut -d " " -f2) $(./LandauerHeatCurrent.out | tail -n4 | head -n1 | cut -d " " -f25) >> scan.txt # send output SI conductance to file
        #echo -n $(head input.txt -n5 | tail -n1 | cut -d " " -f1) $(./LandauerHeatCurrent.out | tail -n4 | head -n1 | cut -d " " -f19) >> scan.txt # send output SI heat current to file
        #echo -n $(head input.txt -n5 | tail -n1) $(./LandauerHeatCurrent.out | tail -n4 | head -n1 | cut -d " " -f19) >> $j.txt # send output SI heat current to file
        echo -n $(head input.txt -n5 | tail -n1) $(./LandauerHeatCurrent-rc.out | tail -n4 | head -n1 | cut -d " " -f25) >> $j.txt # send output SI heat conduction to file
        echo >> $j.txt
}


function printScanTxt(){
echo "-----------------------------------"
echo "scan.txt:"
echo
echo "       T_R           T_L    K(T_R,T_L)=I(T_R,T_L)/(T_R-T_L) K(T_L)=(dI/dT)(T_L)"

if [ -f scan.txt ] ; then cat scan.txt ; else echo "scan.txt not found. Skipping." ; fi

echo "-----------------------------------"
}


function plotIofTvsOmega()
{

	echo "_______________________________________________________________________"
        echo "Plotting temperature dependent heat current versus oscillator frequency"

#echo "Rigid-molecule mode heat current versus oscillator frequency"
#echo "Plotting temperature dependent heat current versus oscillator frequency"
#cat scan.txt 
#gnuplot -e "set term jpeg enhanced fname times-roman size 1200,800 ; set output 'scan.jpeg' ; set title \"Heat current versus oscillator frequency, I_{{/Symbol w}_0}\" ; set xlabel \"Oscillator Frequency, {/Symbol w}_0/[ps^{-1}]\" ; set ylabel \"Heat Current, I/[W]\" ; p 'scan.txt' noti"
#gnuplot -e "set term svg enhanced size 1200,800 ; set output 'KvsT.svg' ; set title \"Rigid-molecule mode heat current versus oscillator frequency, I_{{/Symbol w}_0}\" ; set xlabel \"Oscillator Frequency, {/Symbol w}_0/[ps^{-1}]\" ; set ylabel \"Heat Current, I/[W]\" ; p 'scan.txt' w l lw 2 noti"
#gnuplot -e "set term svg enhanced size 1200,800 ; set output 'KvsT.svg' ; set title \"Heat current versus oscillator frequency, I_{{/Symbol w}_0}\" ; set xlabel \"Oscillator Frequency, {/Symbol w}_0/[ps^{-1}]\" ; set ylabel \"Heat Current, I/[W]\" ; p 'scan.txt' w l lw 2 noti"

#echo "set term svg enhanced size 1200,800 ; set output 'KvsT.svg' ; set title \"Temperature dependent heat current versus oscillator frequency, I_{{/Symbol w}_0}(T)\" ; set xlabel \"Oscillator Frequency, {/Symbol w}_0/[ps^{-1}]\" ; set ylabel \"Temperature dependent heat current, I(T)/[W]\" ; p \\" >> IofTvsOmega.gp

        if [ 1 -eq $bImagemagik ]
		then
		echo "set term svg enhanced ; set output 'KvsT.svg' ; set title \"Temperature dependent heat current versus oscillator frequency, I_{{/Symbol w}_0}(T) -- $molName\" ; set xlabel \"Oscillator Frequency, {/Symbol w}_0/[ps^{-1}]\" ; " > IofTvsOmega.gp
	else
		echo "set term jpeg enhanced ; set output 'KvsT.jpg' ; set title \"Temperature dependent heat current versus oscillator frequency, I(omega0,T) -- $molName\" ; set xlabel \"Oscillator Frequency, omega0/[ps^{-1}]\" ; " > IofTvsOmega.gp
	fi
        echo "set ylabel \"Temperature dependent heat current, I(T)/[W]\" ; set arrow from $nModes,0 to $nModes,1 nohead lt 0 lw 2 ; p \\" >> IofTvsOmega.gp

        i=0

        for j in ${TEMPS[@]}
                do
                echo -n "'$j.txt' w l lw 2 t '$j K'" >> IofTvsOmega.gp
                if [ $[ ${#TEMPS[@]} - 1 ] -gt $i ]
                        then echo -n ", " >> IofTvsOmega.gp
                        else echo >> IofTvsOmega.gp
                fi
                i=$[ 1 + $i ]
        done

        if [ 1 -eq $bGNUPlot ] ; then gnuplot IofTvsOmega.gp ; fi
}



function mkTvsWgnuplotScript(){

#gnuplot -e "set term x11 enhanced size 1000,600 ; g0=1508 ; wD=75.4 ; maxBathPop=g0 * wD**-3 * (2*wD)**2 * exp(-2) ; set multiplot layout 2,1 ; set arrow from 2.03705,0 to 2.03705,maxBathPop lc 3 ; set arrow from 2.49766,0 to 2.49766,maxBathPop lc 3 ; set arrow from 30.624,0 to 30.624,maxBathPop lc 3 ; set arrow from 32.0797,0 to 32.0797,maxBathPop lc 3 ; set arrow from 136.802,0 to 136.802,maxBathPop lc 3 ; set arrow from 228.027,0 to 228.027,maxBathPop lc 3 ; p 'transmission.txt' u 1:(maxBathPop*column(2)) t 'Transmission function (normalized to maximum bath population)', g0 * wD**-3 * x**2 * exp(-x/wD) t 'Bath density of states' ; unset arrow ; set style data lines ; set logscale y ; p '50.txt', '300.txt', '1000.txt' ; unset multiplot ; pause -1"
		#gnuplot -e "set term svg enhanced ; set output "FrequencyDependence.svg" ; g0=1508 ; wD=75.4 ; maxBathPop=g0 * wD**-3 * (2*wD)**2 * exp(-2) ; set multiplot layout 2,1 ; set arrow from 2.03705,0 to 2.03705,maxBathPop lc 3 ; set arrow from 2.49766,0 to 2.49766,maxBathPop lc 3 ; set arrow from 30.624,0 to 30.624,maxBathPop lc 3 ; set arrow from 32.0797,0 to 32.0797,maxBathPop lc 3 ; set arrow from 136.802,0 to 136.802,maxBathPop lc 3 ; set arrow from 228.027,0 to 228.027,maxBathPop lc 3 ; p 'transmission.txt' u 1:(maxBathPop*column(2)) t 'Transmission function (normalized to maximum bath population)', g0 * wD**-3 * x**2 * exp(-x/wD) t 'Bath density of states' ; unset arrow ; set style data lines ; set logscale y ; p '50.txt', '300.txt', '1000.txt' ; unset multiplot"

		if [ 1 -eq $bImagemagik ]
			then
			echo "set term svg enhanced size 1152,864 ; set output 'FrequencyDependence.svg' ; " > tvsw.gp
		else
	                echo "set term jpeg enhanced size 1152,864 ; set output 'FrequencyDependence.jpg' ; " > tvsw.gp
		fi
		echo "set title \"$molName\" ; g0=$(head -n3 input.txt | tail -n1 | cut -d ' ' -f1) ; wD=$(head -n2 input.txt | tail -n1) ; maxBathPop=4 * exp(-2) * g0 / wD ; set multiplot layout 2,1 ; " >> tvsw.gp

		#declare -a eigenfreq
		#OLDIFS=$IFS # internal field separator
		IFS=$' '
		eigenfreq=($(head -n5 input.txt | tail -n1))
		#IFS=$OLDIFS
		unset IFS
		echo -e "\neigenfrequencies / [radians per ps]: ${eigenfreq[@]}"
                nFreq=${#eigenfreq[@]}
                lastFreq=$[ $nFreq - 1 ]
                maxFreq=${eigenfreq[ $lastFreq ]}
                echo -e "no. of eigenfrequencies:		 ${#eigenfreq[@]} = $maxFreq\n"

		maxTransmissionValue=1 #$natoms*3 #*$natoms*3
		for eif in ${eigenfreq[@]}
			do 
			#echo "DBG: drawing eigenfrequency $eif"
			#echo "set arrow from $eif,0 to $eif,maxBathPop lc 3 ; " >> tvsw.gp
                        echo "set arrow from $eif,0 to $eif,$maxTransmissionValue lc 1 lw 2; " >> tvsw.gp
		done

		#echo "set label at graph 0.75,0.8 'Molecular VDOS in blue' textcolor rgb 'blue' ; p 'transmission.txt' u 1:(maxBathPop*column(2)) t 'Transmission function (normalized to maximum bath population)', (g0 / wD) * (x / wD)**2 * exp(-x/wD) t 'Bath density of states' ; unset arrow ; unset label ; set title 'Frequency dependence of the heat current about various temperatures' ; set style data lines ; set logscale y ; p \\" >> tvsw.gp
                echo "set label at graph 0.85,0.8 left 'Molecular VDOS in red' textcolor rgb 'red' ; set ylabel 'Transmission / [normalized for a single mode]' ; set y2label 'Density Of States, scaled by a factor of {/Symbol g}(0) = '.sprintf(\"%g\",g0).'\\n/ [number of modes per rad ps^{-1}]' ; set xlabel 'angular frequency / [radians / picoseconds]' ; set datafile separator  ',' ; p [:][0:$nModes+1] 1 lt 0 noti, $nModes lt 0 noti, g0 * wD**(-3) * x**2 * exp(-x/wD) w l t 'Bath Density Of States scaled by a factor of {/Symbol g}(0) = '.sprintf(\"%g\",g0), 'transmission.txt' w l t 'Junction transmission function' ; unset arrow ; unset label ; unset y2label ; set title 'Frequency dependence of the heat current about various temperatures' ; set ylabel 'Power Spectral Density/[W/  rad per ps]' ; set style data lines ; set logscale y ; unset datafile ; p \\" >> tvsw.gp


		nj=0
                for j in ${TEMPS[@]}
                        do 
				nj=$[ 1 + $nj ] # incr nj
                                echo -n "'$j.txt' t '$j K'" >> tvsw.gp
                                if [ ${#TEMPS[@]} -gt $nj ]
                                        then echo ", \\" >> tvsw.gp
                                else
                                        echo " ; " >> tvsw.gp
                                fi
                done

		echo "unset multiplot" >> tvsw.gp
}



#MAIN()
if [ "hydrogen.tau.ac.il" != $(hostname) ] && [ "power.tau.ac.il" != $HOSTNAME ]
	then 
	echo "Working on remote node:"
	pwd
	ls
	if [ ! -z $PBS_O_WORKDIR ] 
		then
		remoteNd
		cd $PBS_O_WORKDIR
		#rm scan.txt
		pwd
		ls
	else
	        echo "PBS_O_WORKDIR=$PBS_O_WORKDIR is not defined. Aborting."
	        exit 129
	fi
#else cd ~/tests/LandauerHeatCurrent
else
	#ulimit -St 100 # soft limit on CPU time (in seconds)
	ulimit -St 10000
	ulimit -Sv 1000000 # soft limit on virtual memory (in KB)
fi

if [ ! -f input.txt ] ; then echo "Required input.txt file missing! Aborting." ; exit 87 ; fi

echo -e "\n*================================================*\n$(date)\n"
set +e
gnuplot --version
if [ 0 -eq $? ] 
	then 
	bGNUPlot=1
	convert --version
	if [ 0 -eq $? ] ; then bImagemagik=1 ; else echo "Imagemagik unavailable. Conversion of graphics will be skipped." ; bImagemagik=0 ; fi
else 
	echo "GNUPlot unavailable. Plotting will be skipped."
	bGNUPlot=0
	bImagemagik=0
fi
set -e


echo
bRecompile=0
bOverwriteCPP=0
bOverwriteScanTxt=1

check4Recompilation

if [ 1 -eq $bRecompile ] && [ "hydrogen.tau.ac.il" == $(hostname) ] ; then recompile ; fi

if [ ! -f LandauerHeatCurrent-rc.out ] ; then echo "LandauerHeatCurrent-rc.out is missing! Aborting." ; exit 129 ; fi

if [ 1 -ne $bOverwriteScanTxt ] && [ -f scan.txt ] ; then echo "scan.txt already exists! Aborting." ; exit 98 ; else if [ -f scan.txt ] ; then rm scan.txt ; fi ; fi

molName=$(echo $PWD | awk -F "/" '{ print $NF }')
natoms=$(head -n1 input.txt)
nModes=$natoms*3

MIN_FREQ_ABS_TOLERANCE="1e-6"
minFreq=$(head -n5 input.txt | tail -n1 | cut -d' ' -f1)
if [ 1 -eq $(echo $minFreq $MIN_FREQ_ABS_TOLERANCE | awk '{print (($1 < $2) ? 1 : 0)}') ]
	then
	echo "ERROR Lowest frequency too close to zero! $minFreq < $MIN_FREQ_ABS_TOLERANCE === MIN_FREQ_ABS_TOLERANCE"
	exit 298
fi

echo "__________________________"
echo "Setting scan parameters..."

NFREQ=0                 # nubmer of oscillator FREQuencies to calculate for
DFREQ=3                 # Difference between consecutive oscillator FREQuencies

#K2invps="0.479924" # Kelvin to inverse picoseconds
K2radinvps="0.130920" # Kelvin to radians per picosecond

delta_T="0.001" # temperature bias in Kelvin
dT=$(echo $delta_T $K2radinvps | awk '{print($1 * $2)}')

declare -a TEMPS
#TEMPS=(77 300 1000) # cold bath temperature in Kelvin, or use externally supplied parameter already in input.txt

declare bExtenalScan=false
if [ 0 -eq ${#TEMPS[@]} ]
	then
	bExtenalScan=true
	T_L=$(head -n4 input.txt | tail -n1 | cut -d' ' -f1)
	T_R=$(head -n4 input.txt | tail -n1 | cut -d' ' -f2)
	dT=$(echo $T_L $T_R | awk '{print($1 - $2)}')
	TEMPS=( ${TEMPS[@]} $T_R )
fi

echo "______________________________________________________"
echo "Scanning over temperature and/or oscillator frequnecy:"

for j in ${TEMPS[@]}
	do
	echo "$j K starting:"
	if [ ! $bExtenalScan ]
		then
		T_R=$(echo $j     $K2radinvps | awk '{printf("%.7e",$1*$2)}')
		T_L=$(echo $j $dT $K2radinvps | awk '{printf("%.7e",($1 + $2) * $3)}')
		echo "T_L=$T_L T_R=$T_R"
		sed -i '4s/.*/'$T_L\ $T_R'/' input.txt
	fi

	if [ 0 -lt $NFREQ ]
		then
		if [ -f $j.txt ] ; then echo "$j.txt already exists! back it up before running again... aborting!" ; exit 99 ; fi
		for i in $(seq 1 $NFREQ)
        		do
			doSingleOscillatorFrequency
		done
        else
		if [ 1 -ne $bOverwriteScanTxt ] && [ "${TEMPS[0]}" == "$j" ] && [ -f scan.txt ] ; then echo "scan.txt already exists! back it up before running again... aborting!" ; exit 102 ; fi
                #echo -n $j $(./LandauerHeatCurrent.out | tail -n4 | head -n1 | cut -d " " -f25) >> scan.txt
		if [ 1 -lt ${#TEMPS[@]} ] || [ $bExtenalScan ]
			then
                        if [ -f transmission.txt ] ; then rm transmission.txt ; fi # since this file is only ever appended to in the *.cpp file.
	                #echo -n $(echo $j $dT | awk '{print($1+$2)}') $j $(./LandauerHeatCurrent-rc.out | tail -n6 | head -n1 | cut -d " " -f9) >> scan.txt # differential conductance in pW/K: ... | tail -n4 | head -n1 | cut -d ' ' -f12
			#echo "Beginning to calculate the Landauer heat current for $j K:"

                        ./LandauerHeatCurrent-rc.out 1>> tempOutput.txt
			#./LandauerHeatCurrent-rc.out 2>LandauerHeatCurrent-rc_$dT-$j.log | tail -n6 >> tempOutput.txt

                        #echo "Done calculating Landauer heat current for $j K."
			echo -n $(echo $j $dT | awk '{print($1+$2)}') $j $(tail -n7 tempOutput.txt | head -n1 | cut -d " " -f9) $(tail tempOutput.txt -n5 | head -n1 | cut -d " " -f12)>> scan.txt
			
			echo
			echo -n $(echo $j $dT | awk '{print($1+$2)}') $j
			echo
			tail -n9 tempOutput.txt | head -n5
			echo

			rm tempOutput.txt
                	echo >> scan.txt
			mv output.txt $j.txt
		else
			./LandauerHeatCurrent-rc.out
		fi
        fi
	#echo "$j K done."
done

printScanTxt

if [ 0 -lt $NFREQ ]
	then
	plotIofTvsOmega
else
	echo "________________________________________________"
	echo "Plotting heat conductance temperature dependence"

        mkTvsWgnuplotScript # create the GNUPlot script anyway, even if it can't be executed by this script...

        if [ 1 -eq $bImagemagik ]
		then
		echo "set term svg enhanced size 1920,1080 ; set output 'Non-interactingModes.svg'" > nonInterModes.gp
	else
                echo "set term jpeg enhanced size 1920,1080 ; set output 'Non-interactingModes.jpg'" > nonInterModes.gp
	fi

        echo "set title \"$molName\" ; nModes=$nModes ; wc=75.4 ; bathDOSscale=2*wc*nModes ; MAG=12 ; BASE=1.1 ; set datafile separator ',' ; set multiplot layout 2,1 ; set xlabel 'Frequency/[radians per picosecond]' ; set style fill transparent solid 0.1 ; \
\
set title 'Diagonal-part transmission as a sum of contributions from non-interacting modes' ; plot [:][0:nModes+1]  bathDOSscale * wc**(-3) * x**2 * exp(-abs(x)/wc) w filledcu x1 t 'Bath Density Of States (scaled by 2*{/Symbol w}_c * #modes)', for [i=2:(nModes+2)] 'NoninteractingModesTransmission.txt' u 1:i w l lw MAG*BASE**(-1*i) t (i<nModes+2?'mode '.(i-1): (i == nModes + 2) ? 'Diagonal part (sum over non-interacting modes)' : ''), 'transmission.txt' w l lt 0 lc rgb 'black' lw MAG*BASE**(-1*(nModes+3)) t 'Junction (i.e. including full effects of mode coupling)', 1 lt 2 lc 0 noti, nModes lt 2 lc 0 noti ; \
\
set title 'Heat transfer functions: Transmission(frequency) * frequency / 2 * PI' ; p for [i=2:(nModes+1)] 'NoninteractingModesTransmission.txt' u 1:(column(i)*column(1)/(2*pi)) w l lw MAG*BASE**(-1*i) t (i<nModes+2?'mode '.(i-1) : ''), 'transmission.txt' u 1:(column(2)*column(1)/(2*pi)) w l lt 0 lc rgb 'black' lw MAG*BASE**(-1*(nModes+2)) t 'Junction (i.e. including full effects of mode coupling)' ; \
\
set origin 0.4,0.6 ; set size 0.2,0.3
#clear
#set object 1 rect from screen 0.4, screen 0.5 to screen 0.6, screen 0.8 back behind ; set object 1 rect fc rgb 'white' fillstyle solid 1.0
#set lmargin 6; set bmargin 1.5; set rmargin 0 ; set tmargin 0
#unset key
set xlabel 'Frequency/[radians per picosecond]'
set title 'Low frequency behaviour of Junction transmission (i.e. including full effects of mode coupling)'
plot [0:2] 'transmission.txt' w l lt 0 lc rgb 'black' lw MAG*BASE**(-1*(nModes+3)) noti ; \
\
unset multiplot" >> nonInterModes.gp

	if [ 1 -eq $bGNUPlot ]
		then
			gnuplot --version

        	#echo "set term svg enhanced ; set output \"KvsT.svg\" ; set title \"Heat conduction temperature dependence, {/Symbol K}T)\" ; set xlabel \"Temperature/[K]\" ; set ylabel \"Heat conductance, {/Symbol K}(T)/[W/K]\" ; p \"scan.txt\" u 1:3 w p pt 7 noti" > KvsT.gp ; if [ $bGNUPlot ] ; then gnuplot KvsT.gp ; fi
		#gnuplot -e "set term svg enhanced ; set output 'KvsT.svg' ; set title 'Heat conduction temperature dependence, {/Symbol K}T )' ; set xlabel 'Temperature/[K]' ; set ylabel 'Heat conductance, {/Symbol K} ( T ) /[W/K]' ; p 'scan.txt' u 1:3 w p pt 7 noti" 
		if [ 1 -eq $bImagemagik ]
			then
			echo "set term svg enhanced ; set output 'KvsT.svg' ; set title 'Heat conduction temperature dependence, {/Symbol K}T ) -- $molName' ; set xlabel 'Temperature/[K]' ; set ylabel 'Heat conductance, {/Symbol K} ( T ) /[W/K]' ; " > KvsT.gp
		else
			echo "set term jpeg enhanced ; set output 'KvsT.jpg' ; set title 'Heat conduction temperature dependence, K(T) -- $molName' ; set xlabel 'Temperature/[K]' ; set ylabel 'Heat conductance, K(T) /[W/K]' ; " > KvsT.gp
		fi
		echo "p 'scan.txt' u 1:3 w p pt 7 noti" >> KvsT.gp

		gnuplot KvsT.gp
		if [ 0 -eq $? ] ; then rm KvsT.gp scan.txt ; fi

		gnuplot nonInterModes.gp
                if [ 0 -eq $? ]
                        then
			if [ 0 -eq $? ]
	                        then
                                rm nonInterModes.gp NoninteractingModesTransmission.txt
                        else
                                echo "Imagemagik erro with 'convert Non-interactingModes.svg Non-interactingModes.jpg' !"
                        fi
			if [ 1 -eq $bImagemagik ]
	                        then
        	                if [ -f Non-interactingModes.svg ] ; then set +e ; convert Non-interactingModes.svg Non-interactingModes.jpg ; rm Non-interactingModes.svg ; if [ 0 -ne $? ] ; then bImagemagik=0 ; fi ; set -e ; fi
			else
				echo "(Imagemagik not found, therefore skipping conversion of *.svg files to *.jpeg format)"
			fi
                else
                        cat nonInterModes.gp
                fi

		# Plot frequency dependence of transmission function, bath density of modes, and molecular density of modes (VDOS) and of heat current at different temperatures.
		gnuplot tvsw.gp
		if [ 0 -eq $? ]
			then
			rm transmission.txt
			for j in ${TEMPS[@]}
                        	do
                                rm $j.txt
	                done
		fi
		
		if [ 1 -eq $bImagemagik ]
			then 
			if [ -f KvsT.svg ] ; then set +e ; convert KvsT.svg KvsT.jpg ; set -e ; fi
			if [ 0 -eq $? ] ; then rm KvsT.svg ; fi
			if [ -f FrequencyDependence.svg ] ; then set +e ; convert FrequencyDependence.svg FrequencyDependence.jpg ; set -e ; fi
                        if [ 0 -eq $? ] ; then rm FrequencyDependence.svg ; fi
		else
			echo "(Imagemagik not found, therefore skipping conversion of *.svg files to *.jpeg format)"
		fi # bImagemagik
                rm tvsw.gp

	else
		echo "(GNUPlot not found, therefore skipping plotting of *.gp files)"
        fi # bGNUPlot

fi # 0 -lt $NFREQ 

if [ "hydrogen.tau.ac.il" != $(hostname) ]
	then
	echo "__________________________"
	echo "Present working directory:"
	echo "PWD=$PWD"
	ls
fi

echo "_____________________________________________________________________________"
echo "#Normal termination of Scan Over Temperature and/or Oscillator Frequency script."
exit 0
