// the following (/** **/) is a multiline DOxygen description of this file:
/**
 * @mainpage  Program for calculation of the Landauer expression of the heat current
 *
 *            Reproduces the work of Dvira & Nitzan, 2003.
 *
 * @file
 * @author Inon Sharony <InonShar@TAU.ac.IL>
 * @date 2014
 * @version 0.1
 *
 * @brief Program for calculation of the Landauer heat current.
 * @details Main file.
 *
 * @see http://atto.tau.ac.il/~inonshar
 *
 * @attention This program is intended for academic purposes only.
 * Any commercial use must be specifically authorized in advance by the author.
 */

/*

 Edit the doxygen configuration file (a.k.a. doxyfile) and set the following tags:

 # The PROJECT_NAME tag is a single word (or sequence of words) that should
 # identify the project. Note that if you do not use Doxywizard you need
 # to put quotes around the project name if it contains spaces.

 PROJECT_NAME           = "Landauer Heat Current"

 # The PROJECT_NUMBER tag can be used to enter a project or revision number.
 # This could be handy for archiving the generated documentation or
 # if some version control system is used.

 PROJECT_NUMBER         =

 # Using the PROJECT_BRIEF tag one can provide an optional one line description
 # for a project that appears at the top of each page and should give viewer
 # a quick idea about the purpose of the project. Keep the description short.

 PROJECT_BRIEF          = "Program to calculate the Landauer expression for the heat current"

 # With the PROJECT_LOGO tag one can specify an logo or icon that is
 # included in the documentation. The maximum height of the logo should not
 # exceed 55 pixels and the maximum width should not exceed 200 pixels.
 # Doxygen will copy the logo to the output directory.

 PROJECT_LOGO           =

 *
 */

// include header files, not .cpp files, otherwise Eclipse will tell the preprocessor to copy the content of the included (.cpp) file AND KEEP (and also compile) the .cpp file. A header file won't be compiled separately after it's included.
//compile with "g++ src/LandauerHeatCurrent.cpp -v -I /usr/include/eigen3 -g3 -O3 -pg -Wall -Wextra -o bin/LandauerHeatCurrent-rc.out"
#include <time.h>
#include "tests.h"

//MAIN
//====
/**
 * Main Landauer Heat Current program.
 *
 * @return Termination normal / abnormal.
 * @retval ReturnVariableValue -- significance
 * 	<ul>
 * 		<li> 0 if terminated normally,</li>
 * 		<li> nonzero integer if terminated abnormally.</li>
 * 	</ul>
 */
int main()
{
	const bool g = false, g2 = false, g3 = false, gCalcOmegaDomain = true, gInput = false;
	const bool gDiagonalPart = true;

	const bool gResonance = false; ///< Print debugging information at the molecular resonance frequencies?
	bool gEigenfreq = false; ///< Is the current frequency being calculated for a resonance frequency?

	const bool bTest = false; ///< Do tests only?

	unsigned short k;

	// INPUT VARIABLES
	ifstream inputFile;
	unsigned short n = 2; ///<no. of atoms
	bool isLinear = (1 == DIM) ? true : false; ///<is this molecule linear?
	bool bRigidBodyDOF = false; ///< if 'true', calculate number of rigid body degrees of freedom, otherwise leave as zero
	unsigned short rigidBodyDOF = 0; ///< number of rigid body degrees of freedom
	//unsigned j, k; ///<degree of freedom (atomic/normal mode) index
	double omegaDebye = 0;
	double * gammasLocal = new double[NBATHS]; ///<local system-bath couplings in units of frequency
	//gammasLocal={	1,1};
	double *temps = new double[NBATHS]; ///<temperatures in units of frequency
	//temps={	1, 0};
	double *eigenfreq = NULL; ///< list of eigenfrequencies
	double **eigenvec = NULL; ///< list of eigenvector coefficients, X[row_index][column_index] format
	double dOmega = 0; ///<interval between consecutive calculation frequencies
	unsigned nFreq = 0; ///<number of frequencies at which to calculate. Could be 'long'.

	//FREQUENCY INDEPENDENT AUXILLIARY VARIABLES
	double * omegaGammas = new double[NBATHS]; ///< frequency dependent couplings

	//FREQUENCY DEPENDENT AUXILLIARY VARIABLES
	const bool bAssumeReversible = true; ///<if false, scan over omega in (-inf,inf), else in (0,inf) and multiply results by two.
	double omega = 0; ///<frequency at which a calculation is preformed
	unsigned firstFreq = 1;
	//unsigned i; ///< frequency index. Could be 'long', should be changed to signed, if we wanted to integrate over (-inf,inf) explicitly.
	int i; ///< bath index
	unsigned short j; ///< normal mode index
	double *sqDelta = NULL; ///<array of differences in squares of a given frequency from the eigenfrequencies

	//MatrixXcd * nmGammaMat = new MatrixXcd[NBATHS](DIM * n, DIM * n); // ISO C++ forbids initialization in array new
	MatrixXcd * nmGammaMat = new MatrixXcd[NBATHS]; ///<array of couplings between normal-modes and each bath
	MatrixXcd sumNMgammaMat; ///<imaginary part of the self-energy (up to a factor of omega...)

	MatrixXcd inverseResolvent; ///<Equal to (-omega + Hamiltonian + SelfEnergy)
	MatrixXcd greens; ///<Dynamically allocated complex double-precision.
	MatrixXcd transmissionMat; ///<transmission matrix (Meir-Wingreen expression)

	double transmission = 0; ///< the value of the transmission at a certain frequency
	double *pops = new double[NBATHS]; ///< baths populations
	//pops={	0, 0};
	double dPops = 0;		///< difference in populations (a.k.a. window function)
	double dPopdT = 0;		///< differential of population with respect to temperature
	double omegaCurrent = 0;		///< net heat current Fourier component with frequency \f$\omega\f$
	double omegaDiffConductance = 0;		///< differential heat conductance Fourier component with frequency \f$\omega\f$

	//OUTPUT VARIABLES
	double avgCurrent = 0;		///< time-averaged heat current
	double avgDiffConductance = 0;		///< time-averaged differential heat conductance
	const char * outputFileName = "output.txt"; //TODO consider changing to 'char *' to 'std::string'
	ofstream outputFile;
	ofstream transOutputFile;
	bool bGNUPlot = false;
	ofstream diagonalPartFile;
	if (gDiagonalPart) diagonalPartFile.open("NoninteractingModesTransmission.txt");
	double diagonalPart = 0, sumOverModes = 0, diagonalPartOmegaCurrent = 0, diagonalPartOmegaDiffConductance = 0, diagonalPartAvgCurrent = 0, diagonalPartAvgDiffConductance = 0;

	clock_t myClock = clock();

	//TEST
	if (bTest) {
		cout << endl << "Performing tests..." << endl;
		if (!testAll()) {
			cerr << "Error in tests. Aborting!" << endl;
			exit(1593);
		}
		freeUpHeap_TestsOrProduction(gammasLocal, temps, omegaGammas, pops, nmGammaMat);
		cout << endl << "_________________________________________________________" << endl << "#Normal termination of Landauer heat current TESTS." << endl;

		return 0;
	}

	//BEGIN
	if (3 != DIM) cerr << endl << "Note spacial dimensionality (" << DIM << ") is not 3!" << endl;
	cout << endl << "________________" << endl << "Getting input..." << endl << std::scientific;

	chkIOfiles(inputFile, outputFile, transOutputFile, outputFileName);
	readAtomNo(inputFile, n);
	//
	//sumRowGreens = new complex<double> [DIM * n];
	cout << endl << "____________________________" << endl << "Allocating dynamic memory...";
	eigenfreq = new double[DIM * n];
	sqDelta = new double[DIM * n];
	eigenvec = new double *[DIM * n];
	for (j = 0; j < DIM * n; ++j)
		eigenvec[j] = new double[DIM * n];

	cout << "\tDynamic memory allocation complete!" << endl;
	//
	omegaDebye = readOmegaDebye(inputFile);
	readGammasLocal(inputFile, gammasLocal);
	readTemps(inputFile, temps);
	readFreq(inputFile, n, eigenfreq);
	readVec(inputFile, n, eigenvec);

	for (i = 0; i < NBATHS; ++i)
		nmGammaMat[i] = MatrixXcd::Zero(DIM * n, DIM * n);
	sumNMgammaMat = MatrixXcd::Zero(DIM * n, DIM * n);
	inverseResolvent = MatrixXcd::Zero(DIM * n, DIM * n);
	greens = MatrixXcd::Zero(DIM * n, DIM * n);
	transmissionMat = MatrixXcd::Zero(DIM * n, DIM * n);

	if (gInput) {
		cout << "DBG: n = " << n << " , isLinear = " << isLinear << " , debyeFrequency = " << omegaDebye << " , gammasLocal = " << gammasLocal[0] << " , " << gammasLocal[1]
				<< " , temps = " << temps[0] << " , " << temps[1] << endl;
		for (j = 0; j < DIM * n; ++j) {
			cout << "DBG: j = " << j << "\teigenfreq[" << j << "] = " << eigenfreq[j] << "\teigenvec[j][k] = ( ";
			for (k = 0; k < DIM * n; ++k)
				cout << eigenvec[j][k] << " , ";
			cout << ")" << endl;
		}
	}

	/*dOmega = getDomega(inputFile, dOmega);
	 nFreq = getNfreq(inputFile, nFreq);*/

	inputFile.close();

	cout << endl << "___________________________________________" << endl << "Doing frequency independent calculations..." << endl;

	//rigid-body Degrees Of Freedom
	rigidBodyDOF = bRigidBodyDOF ? calcRigidBodyDOF(n, isLinear) : 0;

	calcOmegaDomain(gCalcOmegaDomain, gammasLocal, n, rigidBodyDOF, eigenfreq, eigenvec, dOmega, nFreq);

	if (g)
		cout << "DBG: n = " << n << " , isLinear = " << isLinear << " , rigidBodyDOF = " << rigidBodyDOF << " , gammasLocal = " << gammasLocal[0] << " , " << gammasLocal[1]
				<< " , temps = " << temps[0] << " , " << temps[1] << endl << "dOmega = " << dOmega << " , nFreq = " << nFreq << endl;

	if (0 == omegaDebye) calcGammaNM(true, n, gammasLocal, eigenvec, sumNMgammaMat, nmGammaMat);

	cout << endl << "_________________________________________" << endl << "Doing frequency dependent calculations..." << endl;
	if (g2) cout << "nFreq = " << nFreq << " , -1 * nFreq = " << -1 * nFreq << endl;
	//for (i = -1 * nFreq; i < nFreq; ++i)
	//for (i = 1; i < nFreq; ++i)
	if ((MIN_OMEGA > eigenfreq[0]) && (MAX_NFREQ != nFreq)) {
		cerr << "MIN_OMEGA = " << MIN_OMEGA << " is larger than the first (lowest?) eigenfrequency = " << eigenfreq[0] << "! Aborting." << endl;
		exit(8907);
	}
	/*firstFreq
	 = (firstFreq > (unsigned) (MIN_OMEGA / dOmega))
	 ? firstFreq
	 : (unsigned) (MIN_OMEGA
	 / dOmega);
	 */
	firstFreq = bAssumeReversible ? 0 : -1 * (int) nFreq;
	for (i = firstFreq; i < (int) nFreq; ++i) {
		omega = i * dOmega;
		if (dOmega > fabs(omega)) continue;
		if (0 == i % (int) ((nFreq - firstFreq) / 10)) {
			myClock = clock() - myClock;
			cerr << myClock << " clock-ticks = ";
			cerr << std::fixed;
			std::cerr.precision(1);		//set percision to 1 (different effects depending on format flag (a.k.a. 'fmtfl'): 'fixed', 'scientific', or (none) )
			std::cerr.width(9);
			cerr << ((float) myClock) / CLOCKS_PER_SEC;
			std::cerr.width(4);
			cerr << " seconds, (approx " << (10 * ((float) myClock) / CLOCKS_PER_SEC) * (nFreq - i) / (nFreq - firstFreq) << " s to go) : omega = ";
			std::cerr.width(5); // set width to 5 characters. Equivalent to 'cout << setw(5)'. This formatting flag is the only one which isn't "sticky", i.e. it's reset after each use.
			cerr << std::fixed << omega; //set 'fixed' format flag: 'precision' is taken as fixed floating-point percision, i.e. to the position of the decimal point (as opposed to significat figures).
			std::cerr.unsetf(std::ios::floatfield); //unset the format flag for the field bitmask 'floatfield'. Equivalent to 'std::resetiosflags(std::ios::floatfield)'.
			//std::cerr.precision();

			cerr << " rad per ps : frequency\t" << i - firstFreq << "\tout of\t" << nFreq - firstFreq << "\t= " << 100 * (i - firstFreq) / (nFreq - firstFreq) << " % done."
					<< endl;
		}

		gEigenfreq = gResonance ? isResonance(n, dOmega, eigenfreq, omega) : 0;
		/*gEigenfreq = (dOmega / 2 > fabs(27.37 - omega))
		 ? true
		 : gEigenfreq;*/
		if (!(g || g2 || g3 || gCalcOmegaDomain || gInput) && 0 == i % ((int) (nFreq / 100)))
			cout << 100 * i / nFreq << "% done.\ti = " << i << ",\tOmega = " << omega << " ps^-1" << endl;

		if (g2) cout << "i = " << i << " , omega = " << omega << endl;
		if (0 != omegaDebye) {
			calcOmegaGammas(gEigenfreq, omegaDebye, gammasLocal, omega, omegaGammas);
			calcGammaNM(gEigenfreq, n, omegaGammas, eigenvec, sumNMgammaMat, nmGammaMat);
		}
		calcSqDelta(n, eigenfreq, omega, sqDelta);

		calcInverseResolvent(gEigenfreq, n, sqDelta, omega, sumNMgammaMat, inverseResolvent);
		calcGreens(gEigenfreq, n, eigenfreq, omega, inverseResolvent, greens);
		transmission = calcOmegaTransmission(gEigenfreq, n, omega, nmGammaMat, greens, transmissionMat, transOutputFile);

		if (diagonalPartFile.good()) {
			sumOverModes = 0;
			diagonalPartFile << omega;
			for (j = 0; j < DIM * n; ++j) {
				diagonalPart = 4 * pow(omega, 2) * nmGammaMat[0](j, j).real() * nmGammaMat[1](j, j).real()
						/ (pow(sqDelta[j], 2) + pow(omega, 2) * pow(sumNMgammaMat(j, j).real(), 2));
				diagonalPartFile << "," << diagonalPart;
				sumOverModes += diagonalPart;
			}
			diagonalPartOmegaCurrent = calcOmegaCurrent(false, omega, sumOverModes, dPops, dPopdT, diagonalPartOmegaDiffConductance);
			doAvgCurrent(false, bAssumeReversible, diagonalPartOmegaCurrent, diagonalPartOmegaDiffConductance, dOmega, diagonalPartAvgCurrent, diagonalPartAvgDiffConductance);
			diagonalPartFile << "," << sumOverModes << "," << diagonalPartOmegaCurrent << "," << diagonalPartAvgCurrent << "," << diagonalPartOmegaDiffConductance << ","
					<< diagonalPartAvgDiffConductance << endl;
		}

		calcOmegaPops(gEigenfreq, NBATHS, temps, omega, pops, dPops, dPopdT);
		omegaCurrent = calcOmegaCurrent(gEigenfreq, omega, transmission, dPops, dPopdT, omegaDiffConductance);
		doAvgCurrent(gEigenfreq, bAssumeReversible, omegaCurrent, omegaDiffConductance, dOmega, avgCurrent, avgDiffConductance);
		printOmegaCurrent2File(omega, omegaCurrent, omegaDiffConductance, outputFile); // output to file
	}

	if ((int) nFreq != i) {
		fputs("Error while scanning over frequencies. Aborting.", stderr);
		exit(127);
	}

	cout << endl << "_____________" << endl << "Outputting..." << endl;
	doOutput(dOmega, omegaDebye, gammasLocal, temps, avgCurrent, avgDiffConductance, bGNUPlot, outputFileName);

	outputFile.close();
	transOutputFile.close();

	freeUpHeap_TestsOrProduction(gammasLocal, temps, omegaGammas, pops, nmGammaMat);
	freeUpHeap_ProductionOnly(n, eigenfreq, sqDelta, eigenvec);

	cout << endl << "_________________________________________________________" << endl << "#Normal termination of Landauer heat current calculation." << endl;

	return 0;
}
