#ifndef TESTS_H
#define TESTS_H

#include <vector>
#include "functions.h"

bool testCalcOmegaDomain()
	{
		cout << "Testing calcOmegaDomain()...";
		//calcOmegaDomain(gammasLocal, n, rigidBodyDOF, eigenfreq, eigenvec, dOmega, nFreq);

		cout << "\tnot yet implemented. Skipping." << endl;
		return true;
	}

bool testCalcRigidBodyDOF()
	{
		cout << "Testing calcRigidBodyDOF()...";
		//calcRigidBodyDOF(n, isLinear, rigidBodyDOF);
		cout << "\tnot yet implemented. Skipping." << endl;
		return true;
	}

bool testCalcOmegaGammas()
	{
		cout << "Testing calcOmegaGammas()...";
		//calcOmegaGammas(gEigenfreq, debyeFrequency, gammasLocal,omega, omegaGammas);
		cout << "\tnot yet implemented. Skipping." << endl;
		return true;
	}

bool testCalcGammaNM()
	{
		cout << "Testing calcGammaNM()...";

		unsigned short i, j, k, n = 2;
		double * omegaGammas = new double[NBATHS];
		omegaGammas[0] = 1;
		omegaGammas[1] = 2;
		//double eigenfreq[6] = { 1, 1, 1, 2, 2, 2 };
		double ** eigenvec = new double *[DIM * n];
		for (j = 0; j < DIM * n; ++j)
			{
				eigenvec[j] = new double[DIM * n];
				for (k = 0; k < DIM * n; ++k)
					eigenvec[j][k] = 0;
			}
		/*eigenvec[0][0] = 1;
		 eigenvec[1][1] = 1;
		 eigenvec[3][0] = 1;
		 eigenvec[0][3] = 1;
		 eigenvec[3][3] = -1;*/

		eigenvec[0][0] = eigenvec[0][3] = eigenvec[1][1] = eigenvec[1][3]
		      = eigenvec[2][2] = eigenvec[2][5] = eigenvec[3][0] = eigenvec[4][1]
		            = eigenvec[5][2] = 1;
		eigenvec[3][3] = eigenvec[4][4] = eigenvec[5][5] = -1;

		/*{{  1, 0, 0,  1,  0,  0},
		 {	0, 1, 0,  0,  1,  0},
		 {	0, 0, 1,  0,  0,  1},
		 {	1, 0, 0, -1,  0,  0},
		 {	0, 1, 0,  0, -1,  0},
		 {	0, 0, 1,  0,  0, -1}};*/

		MatrixXcd * gammaNM = new MatrixXcd[NBATHS];
		for (i = 0; i < NBATHS; ++i)
			gammaNM[i] = MatrixXcd::Zero(DIM * n, DIM * n);
		MatrixXcd sumGammaNM;
		sumGammaNM = MatrixXcd::Zero(DIM * n, DIM * n);

		calcGammaNM(true, n, omegaGammas, eigenvec, sumGammaNM, gammaNM);

		cout << "{{  1, 0, 0,  1,  0,  0}," << endl << "{	0, 1, 0, 0, 1, 0},"
		      << endl << "{	0, 0, 1, 0, 0, 1}," << endl
		      << "{	1, 0, 0, -1, 0, 0}," << endl << "{	0, 1, 0, 0, -1, 0},"
		      << endl << "{	0, 0, 1, 0, 0, -1}}" << endl;

		delete[] omegaGammas;
		for (j = 0; j < DIM * n; ++j)
			delete[] eigenvec[j];
		delete[] eigenvec;
		delete[] gammaNM;

		return true;
	}

bool testCalcInverseResolvent()
	{
		cout << "Testing calcInverseResolvent()...";

		const unsigned short n = 2;
		unsigned short j, k;
		double * sqDelta = new double[DIM * n];
		for (j = 0; j < DIM * n; ++j)
			sqDelta[j] = j;
		const double omega = 1;
		MatrixXcd sumGammaNM;
		sumGammaNM = MatrixXcd::Zero(DIM * n, DIM * n);
		for (j = 0; j < DIM * n; ++j)
			for (k = 0; k < DIM * n; ++k)
				{
					//cout << j << k << endl;
					sumGammaNM(j, k) = (double) (abs(j - k) < 3) ? 1 : 0;
				}

		//sumGammaNM = MatrixXcd::Ones(DIM * n, DIM * n);

		cout << endl << "here's sumGammaNM:" << endl << sumGammaNM << endl
		      << endl;
		MatrixXcd inverseResolvent;
		inverseResolvent = MatrixXcd::Zero(DIM * n, DIM * n);

		calcInverseResolvent(true, n, sqDelta, omega, sumGammaNM,
		                     inverseResolvent);

		delete[] sqDelta;

		return true;
	}

bool testCalcSqDelta()
	{
		cout << "Testing calcSqDelta()...";
		//calcSqDelta(n, eigenfreq, omega, sqDelta);

		cout << "\tnot yet implemented. Skipping." << endl;
		return true;
	}

bool testCalcGreens()
	{
		cout << "Testing calcGreens()...";

		const unsigned short n = 2;
		unsigned short j, k;
		const double eigenfreq[6] = { 1, 2, 3, 4, 5, 6 };
		const double omega = 1;
		MatrixXcd inverseResolvent;
		inverseResolvent = MatrixXcd::Zero(DIM * n, DIM * n);
		for (j = 0; j < DIM * n; ++j)
			for (k = 0; k < DIM * n; ++k)
				inverseResolvent(j, k) = (0 < k)
				                                 ? polar((double) j + 1,
				                                         (double) PI / (k + 0.5))
				                                 : j + 1;
		cout << "here's the inverse resolvent:" << endl << inverseResolvent
		      << endl;
		MatrixXcd greens;
		greens = MatrixXcd::Zero(DIM * n, DIM * n);
		calcGreens(true, n, eigenfreq, omega, inverseResolvent, greens);
		//cout << "\tnot yet implemented. Skipping." << endl;
		return true;
	}

bool testCalcTransmissionIdentityMatrixesOnly()
	{
		unsigned n = 2;
		double omega = 1;
		MatrixXcd * gammaNM = new MatrixXcd[NBATHS];
		unsigned short i;
		for (i = 0; i < NBATHS; ++i)
			//gammaNM[i] = MatrixXcd::Zero(DIM * n, DIM * n);
			gammaNM[i].setZero(DIM * n, DIM * n);
		//MatrixXcd greens = MatrixXcd::Zero(DIM * n, DIM * n);
		MatrixXcd greens;
		greens.setZero(DIM * n, DIM * n);
		//MatrixXcd trans = MatrixXcd::Zero(DIM * n, DIM * n);
		MatrixXcd trans;
		trans.setZero(DIM * n, DIM * n);
		double transmission = 0;
		ofstream transOutputFile;
		transOutputFile.open("transTest.txt");

		// gammaNM[0] , gammaNM[1] , greens = unit matrix
		gammaNM[0].setIdentity(DIM * n, DIM * n);
		gammaNM[1].setIdentity(DIM * n, DIM * n);
		greens.setIdentity(DIM * n, DIM * n);

		cout << endl << "Transmission matrix due to identity matrixes only:"
		      << endl << gammaNM[0] << endl << greens << endl << gammaNM[1]
		      << endl << greens.conjugate().transpose() << endl;

		transmission = calcOmegaTransmission(true, n, omega, gammaNM, greens, trans,
		                                transOutputFile);

		delete[] gammaNM;

		return (0 <= transmission && DIM * n >= transmission);
	}

bool testCalcTransmissionGammaOnesDiagonalGreens()
	{
		const unsigned short n = 3;

		MatrixXcd * gammaNM = new MatrixXcd[NBATHS];
		gammaNM[0].setOnes(DIM * n, DIM * n);
		gammaNM[1].setOnes(DIM * n, DIM * n);

		const complex<double> z = polar((double) 1, (double) PI / 3); // z = 0.5 + sqrt(3)/2 * i = 1/(0.5 - sqrt(3)/2 * i)
		MatrixXcd greens(DIM * n, DIM * n);
		greens << z / (double) 2, 0, 0, 0, 0, 0, 0, (double) 2 * z, 0, 0, 0, 0, 0, 0, 0.5
		      * z, 0, 0, 0, 0, 0, 0, (double) 5 * z, 0, 0, 0, 0, 0, 0, z, 0, 0, 0, 0, 0, 0, z;

		cout << endl << "Transmission matrix due to diagonal matrixes only:"
		      << endl << gammaNM[0] << endl << greens << endl << gammaNM[1]
		      << endl << greens.conjugate().transpose() << endl;

		const double omega = 1;
		double transmission = 0;
		MatrixXcd trans(DIM * n, DIM * n);
		ofstream transOutputFile;
		transOutputFile.open("transTest.txt");

		transmission = calcOmegaTransmission(true, n, omega, gammaNM, greens, trans,
		                                transOutputFile);
		return (0 <= transmission && DIM * n >= transmission);
	}

bool testCalcTransmissionTriatomic()
	{
		cout << endl << "*====================================*" << endl;

		const unsigned short n = 3;
		const double omega = 30.63723; //transmission = 9.049235 > 1 !

		MatrixXcd gNM0(DIM * n, DIM * n);
		gNM0 << 10.39704, 0, 12.76220, 0, -7.401040, 0, 0, 0, 0, 0, 10.39704, 0, 12.76220, 0, -7.401040, 0, 0, 0, 12.76220, 0, 15.66540, 0, -9.084658, 0, 0, 0, 0, 0, 12.76220, 0, 15.66540, 0, -9.084658, 0, 0, 0, -7.401040, 0, -9.084658, 0, 5.268364, 0, 0, 0, 0, 0, -7.401040, 0, -9.084658, 0, 5.268364, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10.22951, -12.65896, -7.457002, 0, 0, 0, 0, 0, 0, -12.65896, 15.66540, 9.227999, 0, 0, 0, 0, 0, 0, -7.457002, 9.227999, 5.435928;

		MatrixXcd gNM1(DIM * n, DIM * n);
		gNM1 << 10.39704, 0, -12.76220, 0, -7.401040, 0, 0, 0, 0, 0, 10.39704, 0, -12.76220, 0, -7.401040, 0, 0, 0, -12.76220, 0, 15.66540, 0, 9.084658, 0, 0, 0, 0, 0, -12.76220, 0, 15.66540, 0, 9.084658, 0, 0, 0, -7.401040, 0, 9.084658, 0, 5.268364, 0, 0, 0, 0, 0, -7.401040, 0, 9.084658, 0, 5.268364, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10.22951, 12.65896, -7.457002, 0, 0, 0, 0, 0, 0, 12.65896, 15.66540, -9.227999, 0, 0, 0, 0, 0, 0, -7.457002, -9.227999, 5.435928;

		MatrixXcd grn(DIM * n, DIM * n);
		grn << complex<double> (3.272893e-02, -1.404470e-07), 0, 0, 0, complex<
		      double> (1.782691e-01, 4.468783e-04), 0, 0, 0, 0, 0, complex<double> (
		                                                                            3.272893e-02,
		                                                                            -1.404470e-07), 0, 0, 0, complex<
		      double> (1.782691e-01, 4.468783e-04), 0, 0, 0, 0, 0, complex<double> (
		                                                                            1.595201e-02,
		                                                                            -1.642227e-02), 0, 0, 0, 0, 0, 0, 0, 0, 0, complex<
		      double> (1.595201e-02, -1.642227e-02), 0, 0, 0, 0, 0, complex<
		      double> (1.185812e-02, 2.972549e-05), 0, 0, 0, complex<double> (
		                                                                      6.482716e-02,
		                                                                      -9.458143e-02), 0, 0, 0, 0, 0, complex<
		      double> (1.185812e-02, 2.972549e-05), 0, 0, 0, complex<double> (
		                                                                      6.482716e-02,
		                                                                      -9.458143e-02), 0, 0, 0, 0, 0, 0, 0, 0, 0, complex<
		      double> (-7.209981e-03, -4.779258e-02), 0, complex<double> (
		                                                                  -1.141379e-03,
		                                                                  1.645803e-04), 0, 0, 0, 0, 0, 0, 0, complex<
		      double> (-1.718491e-03, -9.279630e-05), 0, 0, 0, 0, 0, 0, 0, complex<
		      double> (-1.605735e-04, 2.315378e-05), 0, complex<double> (
		                                                                 -5.994985e-04,
		                                                                 -7.973323e-08);

		cout << endl << "Transmission function known to fail:" << endl
		      << "gamma_L:" << endl << gNM0 << endl << "Greens:" << endl << grn
		      << endl << "gamma_R:" << endl << gNM1 << endl << "Greens^H.c.:"
		      << endl << grn.conjugate().transpose() << endl;

		MatrixXcd * gNM = new MatrixXcd[NBATHS];
		//for (i = 0; i < NBATHS; ++i) MatrixXcd gNM[i](DIM * n, DIM * n);
		gNM[0] = gNM0;
		gNM[1] = gNM1;

		MatrixXcd trans;
		trans.setZero(DIM * n, DIM * n);
		ofstream transOutputFile;
		transOutputFile.open("transTest.txt");
		double transmission = calcOmegaTransmission(true, n, omega, gNM, grn, trans,
		                                       transOutputFile);
		return (0 <= transmission && DIM * n >= transmission);
	}

double doCalcTransmissionFromNMgammas(
                                      const bool & g,
                                      const unsigned short & n,
                                      const double * eigenfreq,
                                      const double & omega,
                                      const MatrixXcd * gammaNM,
                                      const MatrixXcd & sumGammaNM)
	{
		double * sqDelta = new double[DIM * n];
		calcSqDelta(n, eigenfreq, omega, sqDelta);

		MatrixXcd inverseResolvent(DIM * n, DIM * n);
		calcInverseResolvent(g, n, sqDelta, omega, sumGammaNM, inverseResolvent);

		MatrixXcd greens(DIM * n, DIM * n);
		calcGreens(g, n, eigenfreq, omega, inverseResolvent, greens);

		MatrixXcd transmissionMat(DIM * n, DIM * n);
		ofstream transOutputFile;
		transOutputFile.open("transTest.txt", ios::app);
		double transmission = calcOmegaTransmission(g, n, omega, gammaNM, greens,
		                                       transmissionMat, transOutputFile);
		transOutputFile.close();
		delete[] sqDelta;
		return transmission;
	}

bool testCalcTransmissionTriatomicDiagonalPartOfFullGammas()
	{
		cout << endl << "*====================================*" << endl;

		const unsigned short n = 3;

		const double eigenfreq[9] = { 2.03705, 2.03705, 2.49766, 2.49766, 30.624,
		                              30.624, 32.0797, 136.802, 228.027 }; //const double eigenfreq[DIM * n];
		const double omega = 30.63723; //transmission ? 9.049235

		MatrixXcd gNM0(DIM * n, DIM * n);
		//gNM0 << 10.39704, 0, 12.76220, 0, -7.401040, 0, 0, 0, 0, 0, 10.39704, 0, 12.76220, 0, -7.401040, 0, 0, 0, 12.76220, 0, 15.66540, 0, -9.084658, 0, 0, 0, 0, 0, 12.76220, 0, 15.66540, 0, -9.084658, 0, 0, 0, -7.401040, 0, -9.084658, 0, 5.268364, 0, 0, 0, 0, 0, -7.401040, 0, -9.084658, 0, 5.268364, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10.22951, -12.65896, -7.457002, 0, 0, 0, 0, 0, 0, -12.65896, 15.66540, 9.227999, 0, 0, 0, 0, 0, 0, -7.457002, 9.227999, 5.435928;
		gNM0 << 10.39704, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10.39704, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15.66540, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15.66540, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5.268364, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5.268364, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10.22951, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15.66540, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5.435928;
		MatrixXcd gNM1(DIM * n, DIM * n);
		//gNM1 << 10.39704, 0, -12.76220, 0, -7.401040, 0, 0, 0, 0, 0, 10.39704, 0, -12.76220, 0, -7.401040, 0, 0, 0, -12.76220, 0, 15.66540, 0, 9.084658, 0, 0, 0, 0, 0, -12.76220, 0, 15.66540, 0, 9.084658, 0, 0, 0, -7.401040, 0, 9.084658, 0, 5.268364, 0, 0, 0, 0, 0, -7.401040, 0, 9.084658, 0, 5.268364, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10.22951, 12.65896, -7.457002, 0, 0, 0, 0, 0, 0, 12.65896, 15.66540, -9.227999, 0, 0, 0, 0, 0, 0, -7.457002, -9.227999, 5.435928;
		gNM1 << 10.39704, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10.39704, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15.66540, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15.66540, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5.268364, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5.268364, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10.22951, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15.66540, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5.435928;
		MatrixXcd * gammaNM = new MatrixXcd[NBATHS];
		gammaNM[0] = gNM0;
		gammaNM[1] = gNM1;

		MatrixXcd sumGammaNM(DIM * n, DIM * n);
		sumGammaNM.setZero(DIM * n, DIM * n);
		unsigned j, k, l;
		for (j = 0; j < DIM * n; ++j)
			for (k = 0; k < DIM * n; ++k)
				for (l = 0; l < NBATHS; ++l)
					sumGammaNM(j, k) += gammaNM[l](j, k);

		double transmission = doCalcTransmissionFromNMgammas(true, n, eigenfreq,
		                                                     omega, gammaNM,
		                                                     sumGammaNM);

		return (0 <= transmission && DIM * n >= transmission);
	}

bool testCalcTransmissionDiatomicIdentityGammas()
	{
		cout << endl << "*====================================*" << endl;

		const unsigned short n = 2;

		const double eigenfreq[6] = { 2.49767, 2.49767, 2.49767, 2.49767,
		                              39.4915, 189.394 }; //const double eigenfreq[DIM * n];
		const double omega = 39.4915;

		MatrixXcd * gammaNM = new MatrixXcd[NBATHS];
		unsigned short i;
		for (i = 0; i < NBATHS; ++i)
			gammaNM[i].setIdentity(DIM * n, DIM * n);

		MatrixXcd sumGammaNM(DIM * n, DIM * n);
		sumGammaNM.setZero(DIM * n, DIM * n);
		unsigned j, k, l;
		for (j = 0; j < DIM * n; ++j)
			for (k = 0; k < DIM * n; ++k)
				for (l = 0; l < NBATHS; ++l)
					sumGammaNM(j, k) += gammaNM[l](j, k);

		double transmission = doCalcTransmissionFromNMgammas(true, n, eigenfreq,
		                                                     omega, gammaNM,
		                                                     sumGammaNM);

		return (0 <= transmission && DIM * n >= transmission);
	}

bool testCalcTransmissionDiatomic1D()
	{
		cout << endl << "*=============*" << endl << "DIM = 1 , n = 2" << endl;

		const unsigned short n = 2;

		const double eigenfreq[2] = { 2, 20 };
		const double omega = eigenfreq[1];

		const double invSq2 = sqrt(2) / 2;
		//const double eigenvec[2][2] = { { invSq2, invSq2 }, { -1 * invSq2, invSq2 } };//row-major order
		double ** eigenvec = new double *[DIM * n];
		unsigned short j;
		for (j = 0; j < DIM * n; ++j)
			eigenvec[j] = new double[DIM * n];
		eigenvec[0][0] = eigenvec[0][1] = eigenvec[1][1] = invSq2;
		eigenvec[1][0] = -1 * invSq2;

		//const double omegaGammas[NBATHS] = { 0.01 * eigenfreq[1], 0.01 * eigenfreq[1] };
		const double omegaGammas[NBATHS] = { 0.1 * eigenfreq[1], 0.1
		      * eigenfreq[1] };

		MatrixXcd sumGammaNM(DIM * n, DIM * n);
		MatrixXcd * gammaNM = new MatrixXcd[NBATHS];
		unsigned short i;
		for (i = 0; i < NBATHS; ++i)
			gammaNM[i].setZero(DIM * n, DIM * n);
		calcGammaNM(true, n, omegaGammas, eigenvec, sumGammaNM, gammaNM);

		double transmission = doCalcTransmissionFromNMgammas(true, n, eigenfreq,
		                                                     omega, gammaNM,
		                                                     sumGammaNM);

		/*======================================================================*/

		/* Disconnected atoms: Diagonal eigenvectors (localized normal modes)*/

		cout << endl << "*-------------------*" << endl
		      << "diagonal eigenvectors" << endl;

		unsigned short k;
		for (j = 0; j < DIM * n; ++j)
			for (k = 0; k < DIM * n; ++k)
				eigenvec[j][k] = (j == k) ? 1 : 0;///<set eigenvector matrix equal to the identity matrix

		calcGammaNM(true, n, omegaGammas, eigenvec, sumGammaNM, gammaNM);

		transmission = doCalcTransmissionFromNMgammas(true, n, eigenfreq, omega,
		                                              gammaNM, sumGammaNM);

		for (j = 0; j < DIM * n; ++j)
			delete[] eigenvec[j];
		delete[] eigenvec;
		delete[] gammaNM;

		return (0 <= transmission && DIM * n >= transmission);
	}

bool testCalcTransmission2uncoupledDegenerateModes()
	{

		cout << endl << "*------------------------------*" << endl
		      << "Two, uncoupled, degenerate modes: Diagonal eigenvectors and gammas"
		      << endl;

		const unsigned short n = 2;
		const double omegaResonance = 20;
		const double eigenfreq[2] = { omegaResonance, omegaResonance };

		const double debyeFrequency = 75.4;
		const double gammasLocal[NBATHS] = { 1508, 1508 };

		double omega, dOmega = 1e-3, omegaInf = 250;

		const bool g = false;
		unsigned short j, k, l;
		double * omegaGammas = new double[NBATHS];
		MatrixXcd sumGammaNM(DIM * n, DIM * n);
		MatrixXcd * gammaNM = new MatrixXcd[NBATHS];
		unsigned short i;
		for (i = 0; i < NBATHS; ++i)
			gammaNM[i].setZero(DIM * n, DIM * n);
		double transmission;
		ofstream transOutputFile;
		transOutputFile.open("transTest.txt");
		transOutputFile.close();

		for (omega = dOmega; omegaInf > omega; omega += dOmega)
			{
				if (0 == int(omega / dOmega) % int((omegaInf / dOmega) / 20)) cerr
				      << 100 * omega / omegaInf << "%" << endl;

				calcOmegaGammas(g, debyeFrequency, gammasLocal, omega, omegaGammas);
				for (j = 0; j < DIM * n; ++j)
					for (k = 0; k < DIM * n; ++k)
						{
							sumGammaNM(j, k) = 0;
							for (l = 0; l < NBATHS; ++l)
								{
									gammaNM[l](j, k) = (j == k) ? omegaGammas[l] / 2 : 0;///<set diagonal gammas
									sumGammaNM(j, k) += gammaNM[l](j, k);
								}
						}

				transmission = doCalcTransmissionFromNMgammas(g, n, eigenfreq,
				                                              omega, gammaNM,
				                                              sumGammaNM);

				if (omegaResonance == omega && DIM * n != transmission) return false;
			}

		delete[] omegaGammas;
		delete[] gammaNM;

		return true;
	}

bool testCalcTransmission()
	{
		cout << "Testing calcTransmission()...";

		switch (DIM)
			{
			case 3:
				if (!testCalcTransmissionIdentityMatrixesOnly()) return false; ///<Transmission matrix due to identity matrixes only

				if (!testCalcTransmissionGammaOnesDiagonalGreens()) return false; ///< gammaNM[0] , gammaNM[1] = matrix of ones; greens = diagonal (1/(a+ib))

				if (!testCalcTransmissionTriatomic()) return false; ///< triatomic where transmission function = 9 at one of the resonanace frequencies

				if (!testCalcTransmissionTriatomicDiagonalPartOfFullGammas()) return false; ///<Starting from diagonal gammas (==> diagonal Resolvent ==> diagonal greens ==> symmertic greens ==> if omega - eigenfreq[i] << eigenfreq[i+/-1] - eigenfreq[i] then trans is proportional to gL*gR/(gL+gR)
				if (!testCalcTransmissionDiatomicIdentityGammas()) return false;
				break;
			case 1:
				if (!testCalcTransmissionDiatomic1D()) return false;
				testCalcTransmission2uncoupledDegenerateModes();
				break;
			default:
				return false;
			}

		return true;
	}

bool testCalcOmegaPops()
	{
		cout << "Testing calcOmegaPops()...";
		//calcOmegaPops(gEigenfreq,n, temps, omega, pops);

		cout << "\tnot yet implemented. Skipping." << endl;
		return true;
	}

bool testCalcOmegaCurrent()
	{
		cout << "Testing calcOmegaCurrent()...";
		//omegaCurrent = calcOmegaCurrent(gEigenfreq, omega, transmission, dPops, dPopdT, omegaCurrent, omegaDiffConductance);
		cout << "\tnot yet implemented. Skipping." << endl;
		return true;
	}

bool testCalcAvgCurrent()
	{
		cout << "Testing calcAvgCurrent()...";
		//avgCurrent = calcAvgCurrent(omegaCurrent, omegaDiffConductance, dOmega, avgCurrent, avgDiffConductance);
		cout << "\tnot yet implemented. Skipping." << endl;
		return true;
	}

bool testPrintOmegaCurrent2File()
	{
		cout << "Testing printOmegaCurrent2File()...";
		//printOmegaCurrent2File(omega, omegaCurrent, omegaDiffConductance, outputFile);
		cout << "\tnot yet implemented. Skipping." << endl;
		return true;
	}

bool testAll()
	{
		cout << "Machine epsilon (double-precision) = " << MCHN_EPSLN_DBL << endl;

		cout << DIM << " spacial dimensions." << endl;
		switch (DIM)
			{
			case 3:
				if (testCalcOmegaDomain() && testCalcRigidBodyDOF()
				      && testCalcGammaNM() && testCalcSqDelta()
				      && testCalcInverseResolvent() && testCalcGreens()
				      && testCalcTransmission() && testCalcOmegaPops()) return true;
				else return false;
				break;
			case 1:
				if (testCalcTransmission()) return true;
				else return false;
				break;
			default:
				return false;
			}
		return false;
	}

#endif //TESTS_H
