#! /usr/bin/python

# Reads frequency dependent tranmission from file and returns the
# maximal transmission and the frequency at which it is achieved.
# Does not use NumPy.

import sys
inputFileName=sys.argv[1] 

print "Read the file 'trasmission.txt' into memory..."
#with open('transTest.txt') as f:
#with open('transmission.txt') as f:
with open(inputFileName) as f:
   transmission = [[float(x) for x in ln.split()] for ln in f]


if 100 > len(transmission) :
    print "\nThe transmission data, as read into memory:"
    print transmission

#print "\nThe (first) frequency at which transmission is globally maximal:"
#print max([max(l) for l in transmission]) # using list comprehension,
#returns the maximal frequency, not frequency where transmission is
#maximal...

print "\nThe globally maximal transmission\n\
(preceded by the frequency at which it is achieved):"
#print max(trans for (freq,trans) in transmission) # using generator
maxTrans=0
for (freq,trans) in transmission:
    if trans > maxTrans:
        x = freq # = transmission[i][0]
        maxTrans = trans # = transmission[i][1]
print x,maxTrans
