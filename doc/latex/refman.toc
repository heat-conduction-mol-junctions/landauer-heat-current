\contentsline {chapter}{\numberline {1}Program for calculation of the Landauer expression of the heat current }{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Landauer\discretionary {-}{}{}Heat\discretionary {-}{}{}Current Module Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Landauer\discretionary {-}{}{}Heat\discretionary {-}{}{}Current Modules}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Landauer\discretionary {-}{}{}Heat\discretionary {-}{}{}Current Namespace Index}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Landauer\discretionary {-}{}{}Heat\discretionary {-}{}{}Current Namespace List}{5}{section.3.1}
\contentsline {chapter}{\numberline {4}Landauer\discretionary {-}{}{}Heat\discretionary {-}{}{}Current File Index}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}Landauer\discretionary {-}{}{}Heat\discretionary {-}{}{}Current File List}{7}{section.4.1}
\contentsline {chapter}{\numberline {5}Landauer\discretionary {-}{}{}Heat\discretionary {-}{}{}Current Page Index}{9}{chapter.5}
\contentsline {section}{\numberline {5.1}Landauer\discretionary {-}{}{}Heat\discretionary {-}{}{}Current Related Pages}{9}{section.5.1}
\contentsline {chapter}{\numberline {6}Landauer\discretionary {-}{}{}Heat\discretionary {-}{}{}Current Module Documentation}{11}{chapter.6}
\contentsline {section}{\numberline {6.1}Functions dealing with initial input.}{11}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Function Documentation}{12}{subsection.6.1.1}
\contentsline {subsubsection}{\numberline {6.1.1.1}chkIOfiles}{12}{subsubsection.6.1.1.1}
\contentsline {subsubsection}{\numberline {6.1.1.2}readAtomNo}{12}{subsubsection.6.1.1.2}
\contentsline {subsubsection}{\numberline {6.1.1.3}readGammasLocal}{13}{subsubsection.6.1.1.3}
\contentsline {subsubsection}{\numberline {6.1.1.4}readOmegaDebye}{13}{subsubsection.6.1.1.4}
\contentsline {chapter}{\numberline {7}Landauer\discretionary {-}{}{}Heat\discretionary {-}{}{}Current Namespace Documentation}{15}{chapter.7}
\contentsline {section}{\numberline {7.1}Eigen Namespace Reference}{15}{section.7.1}
\contentsline {section}{\numberline {7.2}Find\discretionary {-}{}{}Max\discretionary {-}{}{}Transmission Namespace Reference}{16}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Variable Documentation}{16}{subsection.7.2.1}
\contentsline {subsubsection}{\numberline {7.2.1.1}inputFileName}{16}{subsubsection.7.2.1.1}
\contentsline {subsubsection}{\numberline {7.2.1.2}transmission}{16}{subsubsection.7.2.1.2}
\contentsline {subsubsection}{\numberline {7.2.1.3}maxTrans}{16}{subsubsection.7.2.1.3}
\contentsline {subsubsection}{\numberline {7.2.1.4}x}{16}{subsubsection.7.2.1.4}
\contentsline {subsubsection}{\numberline {7.2.1.5}maxTrans}{16}{subsubsection.7.2.1.5}
\contentsline {section}{\numberline {7.3}std Namespace Reference}{17}{section.7.3}
\contentsline {subsection}{\numberline {7.3.1}Detailed Description}{17}{subsection.7.3.1}
\contentsline {chapter}{\numberline {8}Landauer\discretionary {-}{}{}Heat\discretionary {-}{}{}Current File Documentation}{19}{chapter.8}
\contentsline {section}{\numberline {8.1}Find\discretionary {-}{}{}Max\discretionary {-}{}{}Transmission.py File Reference}{19}{section.8.1}
\contentsline {section}{\numberline {8.2}functions.h File Reference}{20}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Detailed Description}{23}{subsection.8.2.1}
\contentsline {subsection}{\numberline {8.2.2}Define Documentation}{24}{subsection.8.2.2}
\contentsline {subsubsection}{\numberline {8.2.2.1}DIM}{24}{subsubsection.8.2.2.1}
\contentsline {subsubsection}{\numberline {8.2.2.2}GCC\_\discretionary {-}{}{}VERSION}{24}{subsubsection.8.2.2.2}
\contentsline {subsubsection}{\numberline {8.2.2.3}MAX\_\discretionary {-}{}{}NFREQ}{24}{subsubsection.8.2.2.3}
\contentsline {subsubsection}{\numberline {8.2.2.4}MAX\_\discretionary {-}{}{}OMEGAINF}{24}{subsubsection.8.2.2.4}
\contentsline {subsubsection}{\numberline {8.2.2.5}MCHN\_\discretionary {-}{}{}EPSLN\_\discretionary {-}{}{}DBL}{24}{subsubsection.8.2.2.5}
\contentsline {subsubsection}{\numberline {8.2.2.6}MIN\_\discretionary {-}{}{}OMEGA}{25}{subsubsection.8.2.2.6}
\contentsline {subsubsection}{\numberline {8.2.2.7}NBATHS}{25}{subsubsection.8.2.2.7}
\contentsline {subsubsection}{\numberline {8.2.2.8}PI}{25}{subsubsection.8.2.2.8}
\contentsline {subsubsection}{\numberline {8.2.2.9}SQ\_\discretionary {-}{}{}RAD\_\discretionary {-}{}{}PER\_\discretionary {-}{}{}SQ\_\discretionary {-}{}{}PICOSEC\_\discretionary {-}{}{}2\_\discretionary {-}{}{}W}{25}{subsubsection.8.2.2.9}
\contentsline {subsubsection}{\numberline {8.2.2.10}TRANS\_\discretionary {-}{}{}ARG\_\discretionary {-}{}{}MAX}{25}{subsubsection.8.2.2.10}
\contentsline {subsubsection}{\numberline {8.2.2.11}TRANS\_\discretionary {-}{}{}IM\_\discretionary {-}{}{}MAX}{26}{subsubsection.8.2.2.11}
\contentsline {subsubsection}{\numberline {8.2.2.12}TRANSMISSION\_\discretionary {-}{}{}MAX}{26}{subsubsection.8.2.2.12}
\contentsline {subsubsection}{\numberline {8.2.2.13}WVNR\_\discretionary {-}{}{}2\_\discretionary {-}{}{}RAD\_\discretionary {-}{}{}PER\_\discretionary {-}{}{}PS}{26}{subsubsection.8.2.2.13}
\contentsline {subsection}{\numberline {8.2.3}Function Documentation}{26}{subsection.8.2.3}
\contentsline {subsubsection}{\numberline {8.2.3.1}calcGammaNM}{26}{subsubsection.8.2.3.1}
\contentsline {subsubsection}{\numberline {8.2.3.2}calcGreens}{26}{subsubsection.8.2.3.2}
\contentsline {subsubsection}{\numberline {8.2.3.3}calcInverseResolvent}{26}{subsubsection.8.2.3.3}
\contentsline {subsubsection}{\numberline {8.2.3.4}calcOmegaCurrent}{27}{subsubsection.8.2.3.4}
\contentsline {subsubsection}{\numberline {8.2.3.5}calcOmegaDomain}{27}{subsubsection.8.2.3.5}
\contentsline {subsubsection}{\numberline {8.2.3.6}calcOmegaGammas}{27}{subsubsection.8.2.3.6}
\contentsline {subsubsection}{\numberline {8.2.3.7}calcOmegaPops}{27}{subsubsection.8.2.3.7}
\contentsline {subsubsection}{\numberline {8.2.3.8}calcOmegaTransmission}{28}{subsubsection.8.2.3.8}
\contentsline {subsubsection}{\numberline {8.2.3.9}calcRigidBodyDOF}{29}{subsubsection.8.2.3.9}
\contentsline {subsubsection}{\numberline {8.2.3.10}calcSqDelta}{29}{subsubsection.8.2.3.10}
\contentsline {subsubsection}{\numberline {8.2.3.11}doAvgCurrent}{29}{subsubsection.8.2.3.11}
\contentsline {subsubsection}{\numberline {8.2.3.12}doGNUPlot}{29}{subsubsection.8.2.3.12}
\contentsline {subsubsection}{\numberline {8.2.3.13}doOutput}{29}{subsubsection.8.2.3.13}
\contentsline {subsubsection}{\numberline {8.2.3.14}freeUpHeap\_\discretionary {-}{}{}ProductionOnly}{30}{subsubsection.8.2.3.14}
\contentsline {subsubsection}{\numberline {8.2.3.15}freeUpHeap\_\discretionary {-}{}{}TestsOrProduction}{30}{subsubsection.8.2.3.15}
\contentsline {subsubsection}{\numberline {8.2.3.16}isResonance}{30}{subsubsection.8.2.3.16}
\contentsline {subsubsection}{\numberline {8.2.3.17}printOmegaCurrent2File}{30}{subsubsection.8.2.3.17}
\contentsline {subsubsection}{\numberline {8.2.3.18}readDomega}{31}{subsubsection.8.2.3.18}
\contentsline {subsubsection}{\numberline {8.2.3.19}readFreq}{31}{subsubsection.8.2.3.19}
\contentsline {subsubsection}{\numberline {8.2.3.20}readNfreq}{31}{subsubsection.8.2.3.20}
\contentsline {subsubsection}{\numberline {8.2.3.21}readTemps}{31}{subsubsection.8.2.3.21}
\contentsline {subsubsection}{\numberline {8.2.3.22}readVec}{31}{subsubsection.8.2.3.22}
\contentsline {section}{\numberline {8.3}Landauer\discretionary {-}{}{}Heat\discretionary {-}{}{}Current.cpp File Reference}{32}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}Detailed Description}{32}{subsection.8.3.1}
\contentsline {subsection}{\numberline {8.3.2}Function Documentation}{33}{subsection.8.3.2}
\contentsline {subsubsection}{\numberline {8.3.2.1}main}{33}{subsubsection.8.3.2.1}
\contentsline {section}{\numberline {8.4}tests.h File Reference}{36}{section.8.4}
\contentsline {subsection}{\numberline {8.4.1}Function Documentation}{37}{subsection.8.4.1}
\contentsline {subsubsection}{\numberline {8.4.1.1}doCalcTransmissionFromNMgammas}{37}{subsubsection.8.4.1.1}
\contentsline {subsubsection}{\numberline {8.4.1.2}testAll}{37}{subsubsection.8.4.1.2}
\contentsline {subsubsection}{\numberline {8.4.1.3}testCalcAvgCurrent}{38}{subsubsection.8.4.1.3}
\contentsline {subsubsection}{\numberline {8.4.1.4}testCalcGammaNM}{38}{subsubsection.8.4.1.4}
\contentsline {subsubsection}{\numberline {8.4.1.5}testCalcGreens}{38}{subsubsection.8.4.1.5}
\contentsline {subsubsection}{\numberline {8.4.1.6}testCalcInverseResolvent}{39}{subsubsection.8.4.1.6}
\contentsline {subsubsection}{\numberline {8.4.1.7}testCalcOmegaCurrent}{39}{subsubsection.8.4.1.7}
\contentsline {subsubsection}{\numberline {8.4.1.8}testCalcOmegaDomain}{39}{subsubsection.8.4.1.8}
\contentsline {subsubsection}{\numberline {8.4.1.9}testCalcOmegaGammas}{39}{subsubsection.8.4.1.9}
\contentsline {subsubsection}{\numberline {8.4.1.10}testCalcOmegaPops}{39}{subsubsection.8.4.1.10}
\contentsline {subsubsection}{\numberline {8.4.1.11}testCalcRigidBodyDOF}{39}{subsubsection.8.4.1.11}
\contentsline {subsubsection}{\numberline {8.4.1.12}testCalcSqDelta}{39}{subsubsection.8.4.1.12}
\contentsline {subsubsection}{\numberline {8.4.1.13}testCalcTransmission}{40}{subsubsection.8.4.1.13}
\contentsline {subsubsection}{\numberline {8.4.1.14}testCalcTransmission2uncoupledDegenerateModes}{40}{subsubsection.8.4.1.14}
\contentsline {subsubsection}{\numberline {8.4.1.15}testCalcTransmissionDiatomic1D}{40}{subsubsection.8.4.1.15}
\contentsline {subsubsection}{\numberline {8.4.1.16}testCalcTransmissionDiatomicIdentityGammas}{41}{subsubsection.8.4.1.16}
\contentsline {subsubsection}{\numberline {8.4.1.17}testCalcTransmissionGammaOnesDiagonalGreens}{41}{subsubsection.8.4.1.17}
\contentsline {subsubsection}{\numberline {8.4.1.18}testCalcTransmissionIdentityMatrixesOnly}{42}{subsubsection.8.4.1.18}
\contentsline {subsubsection}{\numberline {8.4.1.19}testCalcTransmissionTriatomic}{42}{subsubsection.8.4.1.19}
\contentsline {subsubsection}{\numberline {8.4.1.20}testCalcTransmissionTriatomicDiagonalPartOfFullGammas}{42}{subsubsection.8.4.1.20}
\contentsline {subsubsection}{\numberline {8.4.1.21}testPrintOmegaCurrent2File}{43}{subsubsection.8.4.1.21}
\contentsline {chapter}{\numberline {9}Landauer\discretionary {-}{}{}Heat\discretionary {-}{}{}Current Page Documentation}{45}{chapter.9}
\contentsline {section}{\numberline {9.1}Todo List}{45}{section.9.1}
\contentsline {section}{\numberline {9.2}Bug List}{46}{section.9.2}
